Hello Docker
============

### What is it for ?

Usually when you start to learn how to use Docker, you would be told to
use the beginner image : `docker run hello-world`.

But someday, you may realize that the `hello-world` is very very light, if
we compare the size of this image (~13.3kB) to any smallest image you may
build. An image based on Debian-slim will be above 20MB and even an alpine-based
one will be at least 2MB sized.

How can it be possible ? Well… This repository is a sample of how this can be achieved.


### Requirements

All you need to test this is a working docker setup.


### Build the image and test it

```sh
docker build -t hello-docker .
docker run hello-world
```

